def call(body) {
    def pipelineParams= [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body()
    pipeline {
    agent any
    stages {
        stage('Build') {
            parallel {
                stage('Branch A') {
                    agent { label 'build-agent-a' }
                    steps {
                        sh 'mvn clean install -DskipTests'
                    }
                }
                stage('Branch B') {
                    agent { label 'build-agent-b' }
                    steps {
                        sh 'mvn clean install -DskipTests'
                    }
                }
            }
        }
        stage('Test') {
            parallel {
                stage('Branch A') {
                    agent { label 'test-agent-a' }
                    steps {
                        sh 'mvn test'
                    }
                }
                stage('Branch B') {
                    agent { label 'test-agent-b' }
                    steps {
                        sh 'mvn test'
                    }
                }
            }
        }
        stage('Deploy') {
            parallel {
                stage('Branch A') {
                    agent { label 'deploy-agent-a' }
                    steps {
                        sh 'echo "Deploying branch A"'
                    }
                }
                stage('Branch B') {
                    agent { label 'deploy-agent-b' }
                    steps {
                        sh 'echo "Deploying branch B"'
                    }
                }
            }
        }
    }
    post {
        always {
            echo 'Pipeline completed'
        }
    }
}

}