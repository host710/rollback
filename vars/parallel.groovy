def call(body) {
    def pipelineParams= [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body() 
    pipeline {
	    agent any
        stages {
            stage('Parallel Stages') {
                parallel {
                    stage('Stage 1') {
                        steps {
                            echo 'stage 1'
                        }
                    }
                    stage('Stage 2') {
                        steps {
                            echo 'stage 2'
                        }
                    }
                    stage('Stage 3') {
                        steps {
                            echo 'stage 3'
                        }
                    }
                }
            }
        }
    }
}