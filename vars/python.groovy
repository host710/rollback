def call(body) {
    def pipelineParams= [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body() 
    pipeline {
	    agent none
        stages {
            stage('Docker Build') {
    	        agent any
                steps {
      	            sh 'docker build . -t registry.gitlab.com/devops3x/scm/builder-api:Jenkins'
                }
            }
            stage('Docker Push') {
           	    agent any
                steps {
      	            withCredentials([usernamePassword(credentialsId: 'dockerHub', passwordVariable: 'dockerHubPassword', usernameVariable: 'dockerHubUser')]) {
        	            sh "docker login -u ${env.dockerHubUser} -p ${env.dockerHubPassword} registry.gitlab.com"
                        sh 'docker push registry.gitlab.com/devops3x/scm/builder-api:Jenkins'
                    }
                }
            }
        }
    }
}
